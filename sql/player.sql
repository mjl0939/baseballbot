CREATE TABLE IF NOT EXISTS `player` (
  `number` varchar(50) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `is_retired` bit(1) DEFAULT NULL,
  `id` int DEFAULT NULL,
  PRIMARY KEY (`number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;