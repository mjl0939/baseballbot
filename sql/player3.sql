CREATE TABLE IF NOT EXISTS `player3` (
  `number` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) DEFAULT NULL,
  `is_retired` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
