const vm = Vue.createApp({
  data(){
    return{
      message: 'test',
      isRefreshBtnDisabled: false,
      topPlayerList: [
        {name: 'Test', popularity:7},
          {name: 'Test', popularity:7},
          {name: 'Test', popularity:7},
          {name: 'Test', popularity:7},
          {name: 'Test', popularity:7}
      ]
    }
  },
  methods: {
    refreshOnClick (){
      this.topPlayerList = [];
      this.getTopPopPlayers();
    },
    async getTopPopPlayers (){
      try{
        const response = await axios.get('/popularity')
        for(var i = 0; i < response.data.length; i++){
          this.topPlayerList.push({name: response.data[i].name, popularity: response.data[i].popularity })
        }
      }catch (error){
        console.error('ERROR', error);
      }
    }
  }

}).mount('#app')