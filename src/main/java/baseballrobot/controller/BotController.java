package baseballrobot.controller;

import baseballrobot.model.PlayerPopularity;
import baseballrobot.model.TopPopularityPlayers;
import baseballrobot.entity.Player;
import baseballrobot.model.LineWebhookEvent;
import baseballrobot.service.LineBotFollowHandlerService;
import baseballrobot.service.LineBotMessageHandlerService;
import baseballrobot.service.PlayerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.crypto.Mac;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class BotController {

    @Value("${line.channel.secret}")
    private String lineChannelSecret;

    @Autowired
    private LineBotMessageHandlerService messageHandlerService;
    @Autowired
    private LineBotFollowHandlerService followHandlerService;
    @Autowired
    private PlayerService playerService;

    @PostMapping(value = "/topplayers")
    public @ResponseBody ResponseEntity quickReply(@RequestBody String payload) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        TopPopularityPlayers topPopularityPlayers = objectMapper.readValue(payload, TopPopularityPlayers.class);

        log.info(String.valueOf("topPopularityPlayers.getTopPlayerNumber()) = " + topPopularityPlayers.getTopPlayerNumber()));

        List<PlayerPopularity> playerDtoList = new ArrayList<>();

        List<Player> playerList = playerService.findOrderByPopularityTopNumber(topPopularityPlayers.getTopPlayerNumber());

        for (Player p : playerList) {
            PlayerPopularity playerDto = new PlayerPopularity();
            playerDto.setName(p.getName());
            playerDto.setPopularity(p.getPopularity());
            playerDtoList.add(playerDto);
        }

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(playerDtoList);
    }

    @GetMapping(value = "/popularity")
    public @ResponseBody ResponseEntity getPopularity() throws JsonProcessingException {

        List<PlayerPopularity> playerDtoList = new ArrayList<>();

        List<Player> playerList = playerService.findOrderByPopularityTop5();

        for (Player p : playerList) {
            PlayerPopularity playerDto = new PlayerPopularity();
            playerDto.setName(p.getName());
            playerDto.setPopularity(p.getPopularity());
            playerDtoList.add(playerDto);
        }

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(playerDtoList);
    }

    @GetMapping(value = "/image/{id}")
    public @ResponseBody ResponseEntity<byte[]> getImage(@PathVariable String id) {

        Optional<Player> optPlayer = playerService.findById(id);

        if (optPlayer.isEmpty()) {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Player player = optPlayer.get();

        if (player.getPhoto() == null) {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(player.getPhoto());
    }

    @PostMapping("/bot")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity botEventHook(
            @RequestHeader("X-Line-Signature") String xLineSignature, @RequestBody String payload) {

        try {
            Mac mac = HmacUtils.getInitializedMac(HmacAlgorithms.HMAC_SHA_256, lineChannelSecret.getBytes("UTF-8"));
            String hashed = Base64.encodeBase64String(mac.doFinal(payload.getBytes("UTF-8")));

            if (!StringUtils.equals(xLineSignature, hashed)) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            ObjectMapper objectMapper = new ObjectMapper();
            LineWebhookEvent eventDto = objectMapper.readValue(payload, LineWebhookEvent.class);

            if (eventDto == null) {
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }

            dispatchEventHandler(eventDto);

        } catch (UnsupportedEncodingException ex) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (JsonProcessingException ex) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    private void dispatchEventHandler(LineWebhookEvent eventDto) {
        if (!CollectionUtils.isEmpty(eventDto.getEvents())) {
            eventDto.getEvents().forEach(event -> {
                switch (event.getType()) {
                    case "message":
                        messageHandlerService.handleEvent(event);
                        break;
                    case "follow":
                        followHandlerService.handleEvent(event);
                        break;
                }
            });
        }
    }
}
