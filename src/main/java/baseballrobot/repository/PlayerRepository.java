package baseballrobot.repository;

import baseballrobot.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlayerRepository extends JpaRepository<Player, String> {
    List<Player> findAllByName(String name);

    Optional<Player> findById(String number);

    List<Player> findAllByActive(boolean active);

    List<Player> findAll();

    List<Player> findNumberByActive(boolean active);

    @Query(value = "SELECT * FROM player ORDER BY popularity LIMIT 5;", nativeQuery = true)
    List<Player> findOrderByPopularityTop5();

    @Query(value = "SELECT * FROM player ORDER BY popularity DESC LIMIT ?1", nativeQuery = true)
    List<Player> findOrderByPopularityTopNumber(int number);

    @Query(value = "SELECT * FROM player WHERE DATE_FORMAT(BIRTH_DATE, '%m%d')= ?1 ", nativeQuery = true)
    List<Player> findByBirthday(String birthday);

    @Query(value = "SELECT * FROM player WHERE star = 1", nativeQuery = true)
    List<Player> findStar();
//    @Query(value = "SELECT name FROM player ORDER BY number", nativeQuery = true)
//    List<Player> findName();
//    List<String> findName();

}