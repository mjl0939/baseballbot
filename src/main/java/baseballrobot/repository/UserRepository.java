package baseballrobot.repository;

import baseballrobot.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> findAllByUserId(String userId);

    Optional<User> findById(String userId);
}
