package baseballrobot;

import baseballrobot.service.LineBotMessageHandlerService;
import baseballrobot.service.TestService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@SpringBootApplication
@EnableScheduling
public class BaseballRobotApplication {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyStoreException {
        ConfigurableApplicationContext context =
                SpringApplication.run(BaseballRobotApplication.class, args);
//				context.getBean(PitchInformationService.class).findPitchScoreInformation("0000004638","PitchStarting", 2023);
//				context.getBean(PitchInformationService.class).insertPitchInformationToPitchTable();
//				context.getBean(TestService.class).getWhoHasBirthdayTodayBasicFunctionDto();
    }
}
