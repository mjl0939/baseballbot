package baseballrobot.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Entity
@Table(name = "player")
@Component
@Getter
@Setter
public class Player {
    @Id
    @Column(name = "number")
    private String number;
    @Column(name = "name")
    private String name;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "position")
    private String position;
    @Column(name = "throwandbat")
    private String throwandbat;
    @Column(name = "height")
    private Integer height;
    @Column(name = "weight")
    private Integer weight;
    @Column(name = "birth_date")
    private LocalDate birthDate;
    @Column(name = "debut_date")
    private LocalDate debutDate;
    @Column(name = "degree")
    private String degree;
    @Column(name = "nation")
    private String nation;
    @Column(name = "origin_name")
    private String originName;
    @Column(name = "draft_order")
    private String draftOrder;
    @Lob
    @Column(name = "photo", columnDefinition = "MEDIUMBLOB")
    private byte[] photo;
    @Column(name = "popularity")
    private Integer popularity;
    @Column(name = "star")
    private Boolean star;
}