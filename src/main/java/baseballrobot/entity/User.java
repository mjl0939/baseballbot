package baseballrobot.entity;

import baseballrobot.enumerated.AskingStatus;
import baseballrobot.enumerated.PassionAtBatStatus;
import baseballrobot.enumerated.UserStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "user")
@Component
@Getter
@Setter
public class User {
    @Id
    @Column(name = "user_id")
    private String userId;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private UserStatus status;
    @Column(name = "player_name")
    private String playerName;
    @Column(name = "player_information")
    private String playerInformation;
    @Column(name = "list_length")
    private Integer listLength;
    @Column(name = "height")
    private String height;
    @Column(name = "righty_or_lefty")
    private String rightyOrLefty;
    @Column(name = "test")
    private Integer test;
    @Column(name = "pitcher")
    private String pitcher;
    @Column(name = "hitter")
    private String hitter;
    @Enumerated(EnumType.STRING)
    @Column(name = "passion_at_bat_status")
    private PassionAtBatStatus passionAtBatStatus;
    //        @Enumerated(EnumType.STRING)
//        @Column(name = "asking_status")
//        private AskingStatus askingStatus;
    @Column(name = "same_name_player_number")
    private Integer sameNamePlayerNumber;
}