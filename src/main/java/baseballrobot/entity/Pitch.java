package baseballrobot.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "pitch")
@Component
@Getter
@Setter
public class Pitch {
    @Id
    @Column(name = "number_and_year")
    private String numberAndYear;
    @Column(name = "number")
    private String number;
    @Column(name = "name")
    private String name;
    @Column(name = "team_abbr_name")
    private String teamAbbrName;
    @Column(name = "year")
    private Integer year;
    @Column(name = "pitch_starting")
    private Integer pitchStarting;
    @Column(name = "pitch_closer")
    private Integer pitchCloser;
    @Column(name = "complete_games")
    private Integer completeGames;
    @Column(name = "shoutout")
    private Integer shoutout;
    @Column(name = "wins")
    private Integer wins;
    @Column(name = "loses")
    private Integer loses;
    @Column(name = "save_ok")
    private Integer saveOk;
    @Column(name = "save_fail")
    private Integer saveFail;
    @Column(name = "relief_point_cnt")
    private Integer reliefPointCnt;
    @Column(name = "strikeout_cnt")
    private Integer strikeoutCnt;
}
