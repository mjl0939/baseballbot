package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LineEventSource {
    private String type;
    private String userId;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class QuickReplyItemsDto {
        private List<LineEventMessage.QuickReplyButtonDto> items;

        public QuickReplyItemsDto() {
            items = new ArrayList<>();
        }

        public static QuickReplyItemsDto createInstance(List<LineEventMessage.QuickReplyButtonDto> items) {

            QuickReplyItemsDto result = new QuickReplyItemsDto();

            result.items.addAll(items);

            return result;
        }
    }
}
