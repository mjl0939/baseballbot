package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LineEventMessage {
    private String id;
    private String type;
    @Getter
    private String text;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LineReplyDto {
        private String replyToken;
        private List<LineReplyMessage> messages;

        public LineReplyDto() {
            messages = new ArrayList<>();
        }

        public static LineReplyDto createInstance(String replyToken) {
            LineReplyDto result = new LineReplyDto();
            result.replyToken = replyToken;

            return result;
        }
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class QuickReplyActionDto {
        private String type;
        private String label;
        private String text;

        public static QuickReplyActionDto createInstance(String label, String text) {
            QuickReplyActionDto result = new QuickReplyActionDto();
            result.type = "message";
            result.label = label;
            result.text = text;

            return result;
        }
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class QuickReplyButtonDto {
        private String type;
        private QuickReplyActionDto action;

        public static QuickReplyButtonDto createInstance(QuickReplyActionDto action) {
            QuickReplyButtonDto result = new QuickReplyButtonDto();
            result.type = "action";
            result.action = action;

            return result;
        }
    }
}
