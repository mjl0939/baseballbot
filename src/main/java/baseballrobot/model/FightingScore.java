package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FightingScore {
    @JsonProperty("Avg")
    private Double avg;
    @JsonProperty("Obp")
    private Double obp;
    @JsonProperty("Slg")
    private Double slg;
}
