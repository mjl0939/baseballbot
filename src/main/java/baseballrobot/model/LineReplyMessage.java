package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LineReplyMessage {
    private String type;
    private String text;
    private String originalContentUrl;
    private String previewImageUrl;
    private LineEventSource.QuickReplyItemsDto quickReply;

    public static LineReplyMessage createInstance(String type, String text) {
        LineReplyMessage result = new LineReplyMessage();
        result.type = type;
        result.text = text;

        return result;
    }

    public static LineReplyMessage createTextInstance(String text) {
        LineReplyMessage result = new LineReplyMessage();
        result.type = "text";
        result.text = text;

        return result;
    }

    public static LineReplyMessage createQuickReplyInstance(String text, LineEventSource.QuickReplyItemsDto quickReply) {
        LineReplyMessage result = new LineReplyMessage();
        result.type = "text";
        result.text = text;
        result.quickReply = quickReply;

        return result;
    }

    public static LineReplyMessage createImageWithQuickReplyInstance(String originalContentUrl, String previewImageUrl
            , LineEventSource.QuickReplyItemsDto quickReply) {
        LineReplyMessage result = new LineReplyMessage();
        result.type = "image";
        result.originalContentUrl = originalContentUrl;
        result.previewImageUrl = previewImageUrl;
        result.quickReply = quickReply;

        return result;
    }
}
