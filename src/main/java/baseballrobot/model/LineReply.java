package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LineReply {
    private String replyToken;
    private List<baseballrobot.model.LineReplyMessage> messages;

    public LineReply() {
        messages = new ArrayList<>();
    }

    public static LineReply createInstance(String replyToken) {
        LineReply result = new LineReply();
        result.replyToken = replyToken;

        return result;
    }
}
