package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FightingScoreResponse {
    @JsonProperty("Success")
    private Boolean success;
    @JsonProperty("FightingScore")
    private String fightingScore;
}
