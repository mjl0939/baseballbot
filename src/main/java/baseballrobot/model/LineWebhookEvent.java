package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LineWebhookEvent {
    private String destination;
    @Getter
    private List<baseballrobot.model.LineEvent> events;

}
