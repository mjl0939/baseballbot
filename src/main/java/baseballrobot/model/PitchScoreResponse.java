package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PitchScoreResponse {

    @JsonProperty("Success")
    private Boolean success;

    @JsonProperty("PitchScore")
    private String pitchScore;
}
