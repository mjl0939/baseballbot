package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerPopularity {

    @JsonProperty("name")
    private String name;

    @JsonProperty("popularity")
    private Integer popularity;

}
