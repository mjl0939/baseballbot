package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LineEvent {
    @Getter
    private String replyToken;
    private String type;
    private LineEventSource source;
    @Getter
    private LineEventMessage message;

}
