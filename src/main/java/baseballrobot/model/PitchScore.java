package baseballrobot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PitchScore {
    //    @JsonProperty("Number")
//    private String number;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("TeamAbbrName")
    private String teamAbbrName;
    @JsonProperty("Year")
    private Integer Year;
    @JsonProperty("PitchStarting")
    private Integer pitchStarting;
    @JsonProperty("PitchCloser")
    private Integer pitchCloser;
    @JsonProperty("CompleteGames")
    private Integer completeGames;
    @JsonProperty("ShoutOut")
    private Integer shoutout;
    @JsonProperty("Wins")
    private Integer wins;
    @JsonProperty("Loses")
    private Integer loses;
    @JsonProperty("SaveOK")
    private Integer saveOK;
    @JsonProperty("SaveFail")
    private Integer saveFail;
    @JsonProperty("ReliefPointCnt")
    private Integer reliefPointCnt;
    @JsonProperty("StrikeOutCnt")
    private Integer strikeOutCnt;
}
