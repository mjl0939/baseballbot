package baseballrobot.service;

import baseballrobot.entity.User;
import baseballrobot.enumerated.PassionAtBatStatus;
import baseballrobot.enumerated.UserStatus;
import baseballrobot.model.*;
import baseballrobot.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PassionAtBatService {
    @Autowired
    QuickReplyButtonService quickReplyButtonService;
    @Autowired
    private UserService userService;
    @Autowired
    private PlayerService playerService;
    @Value("${cpblwebsite.requestVerificationToken}")
    private String requestVerificationToken;
    @Value("${cpblwebsite.getPitchScoreUrl}")
    private String getPitchScoreUrl;
    @Value("${cpblwebsite.playerEntityString}")
    private String playerEntityString;
    @Value("${cpblwebsite.fightingEntityPrefix}")
    private String fightingEntityPrefix;

    public List<FightingScore> findFightingScoreList(String hitterNumber, String pitcherNumber) throws JsonProcessingException
            , NoSuchAlgorithmException, KeyStoreException {
        try {
            String url = fightingEntityPrefix;
            HttpResponse httpResponse = null;
            HttpClient httpClient = HttpClients
                    .custom()
                    .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
                    .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .build();
            HttpPost httpPost = new HttpPost(url);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            httpPost.addHeader("Requestverificationtoken", requestVerificationToken);
            httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
            String playerEntity = playerEntityString;
            StringEntity stringEntity = new StringEntity(playerEntity, "UTF-8");
            httpPost.setEntity(stringEntity);
            httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            String transformedEntityToString = EntityUtils.toString(httpEntity, "UTF-8");
            ObjectMapper objectMapper = new ObjectMapper();
            FightingScoreResponse fightingScoreResponse = objectMapper.readValue(transformedEntityToString, FightingScoreResponse.class);
            List<FightingScore> fightingScoreList = objectMapper.readValue(fightingScoreResponse.getFightingScore()
                    , new TypeReference<List<FightingScore>>() {
                    });
            return fightingScoreList;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (KeyManagementException e) {
            throw new RuntimeException(e);
        } catch (Exception ex) {
            log.error("BOOM!", ex);
        }
        return null;
    }

    public String simulateAtBatResult(String hitterNumber, String pitcherNumber) throws JsonProcessingException, NoSuchAlgorithmException
            , KeyStoreException {
        try {
            String testHitterPlayerNumber = "0000004638";
            String testPitcherPlayerNumber = "0000000132";
            List<FightingScore> fightingScoreList = findFightingScoreList(hitterNumber, pitcherNumber);
            if (fightingScoreList.isEmpty())
                fightingScoreList = findFightingScoreList(testHitterPlayerNumber, testPitcherPlayerNumber);
            FightingScore fightingScore = fightingScoreList.get(0);
            double onBaseProbability = 0.6;
            double hitProbability = 0.4;
            double twoMoreBaseHitProbability = 0.3;
            double homerunProbability = 0.15;
            double oneOverThree = 0.33;
            double twoOverThree = 0.67;
            double dice = 0;
            dice = Math.random();
            if (dice > onBaseProbability) {
                double secondDice = Math.random();
                if (secondDice > twoOverThree)
                    return Constants.FANNED_ON_THREE_PITCHES;
                if (secondDice < twoOverThree && secondDice > oneOverThree)
                    return Constants.FULL_COUNT_THEN_STRIKE_OUT;
                if (secondDice < oneOverThree)
                    return Constants.SWING_BACK_BACK_NICE_CATCH_ON_WALL;
            }
            if (dice < onBaseProbability && dice > hitProbability) {
                return Constants.WALK;
            }
            if (dice < hitProbability && dice > twoMoreBaseHitProbability) {
                return Constants.SINGLE;
            }
            if (dice < twoMoreBaseHitProbability && dice > homerunProbability) {
                return Constants.DOUBLE;
            }
            if (dice < homerunProbability) {
                return Constants.HOMERUN;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception ex) {
            log.error(Constants.BOOM, ex);
        }
        return null;
    }

    LineReply getPassionAtBatBasicFunctionDto(User user, LineReply replyDto) throws NoSuchAlgorithmException, KeyStoreException, JsonProcessingException {
            user.setStatus(UserStatus.PASSION_AT_BAT);
            userService.save(user);
            return getPassionAtBatDtoByPassionAtBatStatus(user, replyDto);
    }

    LineReply getPassionAtBatDtoByPassionAtBatStatus(User user, LineReply replyDto) throws NoSuchAlgorithmException, KeyStoreException
            , JsonProcessingException {
        String soFarInserted = "";
        String instruction = "";
        LineEventSource.QuickReplyItemsDto quickReply = new LineEventSource.QuickReplyItemsDto();
        List<PassionAtBatStatus> passionAtBatStatusList = new ArrayList<>();
        if (!passionAtBatStatusList.contains(user.getPassionAtBatStatus())) {
            user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_HEIGHT);
            userService.save(user);
        }
        switch (user.getPassionAtBatStatus()) {
            case CHOOSE_HEIGHT:
                instruction = Constants.PLEASE_CHOOSE_YOUR_HEIGHT;
                quickReply = quickReplyButtonService.createChooseHeightQuickReplyButton(user);
                user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_RIGHTY_OR_LEFTY);
                break;
            case CHOOSE_RIGHTY_OR_LEFTY:
                soFarInserted = Constants.SO_FAR_INSERTED_IS + Constants.HEIGHT + user.getHeight() + Constants.CM;
                instruction = Constants.PLEASE_CHOOSE_WHETHER_LEFTY_OR_RIGHTY_YOU_ARE;
                quickReply = quickReplyButtonService.createChooseRightyLeftyQuickReplyButton();
                user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_PITCHER);
                break;
            case CHOOSE_PITCHER:
                soFarInserted = Constants.SO_FAR_INSERTED_IS + Constants.HEIGHT + user.getHeight() + Constants.CM + Constants.AND
                        + user.getRightyOrLefty();
                instruction = Constants.PLEASE_CHOOSE_PITCHER;
                quickReply = quickReplyButtonService.createChoosePitcherQuickReplyButton();
                user.setPassionAtBatStatus(PassionAtBatStatus.SHOW_RESULT);
                break;
            case SHOW_RESULT:
                soFarInserted = soFarInserted + "\n" + simulateAtBatResult("", "");
                quickReply = quickReplyButtonService.createAllFunctionQuickReplyButton(user);
                user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_HEIGHT);
                break;
        }
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(soFarInserted + instruction, quickReply));
        return replyDto;
    }
}
