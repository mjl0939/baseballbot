package baseballrobot.service;

import baseballrobot.entity.Pitch;
import baseballrobot.repository.PitchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PitchService {
    @Autowired
    private PitchRepository pitchRepository;

    @Transactional
    public Pitch save(Pitch pitch) {
        return this.pitchRepository.save(pitch);
    }
}
