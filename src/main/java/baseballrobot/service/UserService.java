package baseballrobot.service;

import baseballrobot.entity.User;
import baseballrobot.enumerated.UserStatus;
import baseballrobot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public Optional<User> findAllByUserId(String userId) {
        return userRepository.findAllByUserId(userId);
    }

    @Transactional
    public Optional<User> findById(String userId) {
        return userRepository.findById(userId);
    }

    @Transactional
    public User save(User user) {
        return this.userRepository.save(user);
    }

    public void userStatusSetClear(User user) {
        user.setStatus(UserStatus.ASKING);
        user.setListLength(0);
        user.setPlayerName("");
        user.setPlayerInformation("");
        save(user);
    }

}
