package baseballrobot.service;

import baseballrobot.model.PitchScore;
import baseballrobot.model.PitchScoreResponse;
import baseballrobot.util.Constants;
import com.fasterxml.jackson.core.type.TypeReference;
import baseballrobot.repository.PlayerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PlayerCpblWebsiteInformationService {

    @Value("${cpblwebsite.requestVerificationToken}")
    private String requestVerificationToken;
    @Value("${cpblwebsite.getPitchScoreUrl}")
    private String getPitchScoreUrl;
    @Value("${cpblwebsite.playerEntityPrefix}")
    private String playerEntityPrefix;
    @Value("${cpblwebsite.playerEntityBottom}")
    private String playerEntityBottom;
    @Autowired
    private PlayerRepository playerRepository;

    public String findInformation(String information, String playerNumber) {

        try {
            String url = getPitchScoreUrl;
            StringBuilder zeropart = new StringBuilder();
            HttpResponse httpResponse = null;

            HttpClient httpClient = HttpClients
                    .custom()
                    .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
                    .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .build();

            HttpPost httpPost = new HttpPost(url);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            httpPost.addHeader("Requestverificationtoken", requestVerificationToken);
            httpPost.addHeader("X-Requested-With", "XMLHttpRequest");


            for (int j = 0; j <= (3 - String.valueOf(playerNumber).length()); j++) zeropart.append("0");
            String playerEntity = playerEntityPrefix + playerNumber + playerEntityBottom;
            StringEntity stringEntity = new StringEntity(playerEntity, "UTF-8");
            httpPost.setEntity(stringEntity);
            httpResponse = httpClient.execute(httpPost);

            HttpEntity httpEntity = httpResponse.getEntity();
            String transformedEntityToString = EntityUtils.toString(httpEntity, "UTF-8");

            ObjectMapper objectMapper = new ObjectMapper();
            PitchScoreResponse pitchScoreResponse = objectMapper.readValue(transformedEntityToString, PitchScoreResponse.class);

            List<PitchScore> pitchScoreList = objectMapper.readValue(pitchScoreResponse.getPitchScore(), new TypeReference<List<PitchScore>>() {
            });

//            return pitchScoreList.get(0).getTotalGames();
            return "";

        } catch (Exception ex) {
            log.error(Constants.BOOM, ex);
        }
        return Constants.NO_PITCH_IN_ROOKIE_YEAR;
    }
}
