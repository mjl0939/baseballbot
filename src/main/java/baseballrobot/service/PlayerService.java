package baseballrobot.service;

import baseballrobot.entity.Player;
import baseballrobot.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerService {
    @Autowired
    private PlayerRepository playerRepository;

    @Transactional
    public Player save(Player player) {
        return this.playerRepository.save(player);
    }

    @Transactional
    public List<Player> findAllByName(String name) {
        return playerRepository.findAllByName(name);
    }

    @Transactional
    public Optional<Player> findById(String number) {
        return playerRepository.findById(number);
    }

    @Transactional
    public List<Player> findAllByActive(boolean active) {
        return playerRepository.findAllByActive(active);
    }

    @Transactional
    public List<Player> findAll() {
        return playerRepository.findAll();
    }

    @Transactional
    public List<Player> findNumberByActive(boolean active) {
        return playerRepository.findNumberByActive(active);
    }

    @Transactional
    public List<Player> findOrderByPopularityTop5() {
        return playerRepository.findOrderByPopularityTop5();
    }

    @Transactional
    public List<Player> findOrderByPopularityTopNumber(int number) {
        return playerRepository.findOrderByPopularityTopNumber(number);
    }

    @Transactional
    public List<Player> findByBirthday(String birthday) {
        return playerRepository.findByBirthday(birthday);
    }

    @Transactional
    public List<Player> findStar() {
        return playerRepository.findStar();
    }

}
