package baseballrobot.service;

import baseballrobot.entity.Player;
import baseballrobot.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Optional;

@Slf4j
@Service
public class PlayerPhotoService {

    @Value("${playerstatistic.webaddress}")
    private String playerstatisticwebaddress;

    @Value("${playerimage.webaddress}")
    private String playerimagewebaddress;

    @Autowired
    private PlayerService playerService;

    public void insertPhotoByNumber(String playerNumber) {
        byte[] byteArray = new byte[1024 * 1024];
        Player player = new Player();
        try {
            HttpClient httpClient = HttpClients
                    .custom()
                    .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
                    .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .build();

            HttpGet httpGet = new HttpGet(searchImageUrlByPlayerNumber(playerNumber));
            HttpResponse httpResponse = httpClient.execute(httpGet);
            byteArray = EntityUtils.toByteArray(httpResponse.getEntity());

            Optional<Player> optplayer = playerService.findById(playerNumber);
            player = optplayer.get();
            player.setPhoto(byteArray);
            playerService.save(player);
        } catch (Exception e) {
            log.error(Constants.BOOM, e);
        }
    }

    public String searchImageUrlByPlayerNumber(String playerNumber) {
        String[] imageUrl = new String[0];
        try {
            Document doc;
            String[] imageUrlDiv;
            String url = playerstatisticwebaddress + playerNumber;
            doc = Jsoup.connect(url).timeout(10000).validateTLSCertificates(false).get();
            imageUrlDiv = doc.select("div.img").select("span").select("span[style]").attr("style").split("\\(");
            String playername = doc.select("div.name").text();
            if (!playername.isEmpty()) {
                imageUrl = imageUrlDiv[1].split("\\)");
            }
        } catch (Exception e) {
            log.error(Constants.BOOM, e);
        }
        return urlChinesePartTransformIntoEncoded(imageUrl[0]);
    }

    public String urlChinesePartTransformIntoEncoded(String url) {
        String urlFinal = "";
        try {
            String urlChinesePart = url.replaceAll("[^\u4e00-\u9fa5]", "");
            String encodedURL = URLEncoder.encode(urlChinesePart, "UTF-8");
            urlFinal = playerimagewebaddress + url.replaceFirst("[\u4e00-\u9fa5]", encodedURL)
                    .replaceAll("[\u4e00-\u9fa5]", "");
            return urlFinal;
        } catch (UnsupportedEncodingException e) {
            log.error(Constants.BOOM, e);
        }
        return urlFinal;
    }
}