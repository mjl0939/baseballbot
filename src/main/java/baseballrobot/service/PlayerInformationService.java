package baseballrobot.service;

import baseballrobot.entity.Player;
import baseballrobot.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PlayerInformationService {
    @Autowired
    private PlayerService playerService;
    @Value("${playerlist.webaddress}")
    private String playerlistwebaddress;
    @Value("${playerstatistic.webaddress}")
    private String playerstatisticwebaddress;

    public void insertPlayerInformationToPlayerTable() {
        try {
            Document doc;
            Document presentactivedoc;
            Elements divs;
            String playername;
            String playernumber;
            String playernumberstring;
            String[] playernamedivided;
            String[] height_and_weight_divided;
            String repalcedPlayername = "";
            String webaddress = playerstatisticwebaddress;
            String url = null;
            String urlzeropart = "";
            String presentresult = "";
            List<String> specialNameList = new ArrayList<>();
            List<String> present_active_player_number_list = new ArrayList<>();
            presentactivedoc = Jsoup.connect(playerlistwebaddress).timeout(10000).validateTLSCertificates(false).get();
            Elements presentactiveListElements = presentactivedoc.select("div.PlayersList");

            for (Element element : presentactiveListElements) {
                Element dlElement = element.select("dl").get(0);
                Elements ddElements = dlElement.select("dd");
                for (Element ddElement : ddElements) {
                    String present_active_player_number = ddElement.select("a").get(0)
                            .attr("href").replaceAll("[^0-9]", "");
                    present_active_player_number_list.add(present_active_player_number);
                }
            }

            int minNotExistPlayerNumber = 1;
            int maxNotExistPlayerNumber = 90;

            for (int i = minNotExistPlayerNumber; i <= maxNotExistPlayerNumber; i++) {

                url = "";
                urlzeropart = "";

                for (int j = 0; j <= (3 - String.valueOf(i).length()); j++) urlzeropart = urlzeropart + "0";
                playernumberstring = String.valueOf(i);//掃描設定
                playernumber = "000000" + urlzeropart + playernumberstring;
                url = webaddress + urlzeropart + "000000" + playernumberstring;

                doc = Jsoup.connect(url).timeout(10000).validateTLSCertificates(false).get();
                divs = doc.select("div.name");//掃描名字與編號
                playername = divs.text();

                if (!(playername.equals("") || playername.equals(null))) {

                    Player player = new Player();
                    player.setNumber(playernumber);

                    playernamedivided = playername.split("(?<=\\D)(?=\\d)");

                    if (!playernamedivided[0].matches("[\u4e00-\u9fa5]+")) {

                        repalcedPlayername = playernamedivided[0].replaceAll("[^\u4e00-\u9fa5]", "");

                        specialNameList.add(repalcedPlayername);

                    } else {

                        repalcedPlayername = playernamedivided[0];
                    }

                    player.setName(repalcedPlayername);

                    height_and_weight_divided = doc.select("dd.ht_wt").select("div.desc").text().split("/");

                    height_and_weight_divided[0] = height_and_weight_divided[0].replaceAll("[^0-9]", "");

                    height_and_weight_divided[1] = height_and_weight_divided[1].replaceAll("[^0-9]", "");

                    if (present_active_player_number_list.contains(playernumber)) {
                        player.setActive(true);
                    } else {
                        player.setActive(false);
                    }

                    player.setPosition(doc.select("dd.pos").select("div.desc").text());

                    player.setThrowandbat(doc.select("dd.b_t").select("div.desc").text());

                    player.setHeight(Integer.valueOf(height_and_weight_divided[0]));

                    player.setWeight(Integer.valueOf(height_and_weight_divided[1]));

                    if (!(doc.select("dd.born").select("div.desc").text().isEmpty()
                            || doc.select("dd.born").select("div.desc").text().equals(null))) {

                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

                        String date = doc.select("dd.born").select("div.desc").text();

                        LocalDate localDate = LocalDate.parse(date, formatter);

                        player.setBirthDate(localDate);
                    }

                    if (!(doc.select("dd.debut").select("div.desc").text().isEmpty()
                            || doc.select("dd.debut").select("div.desc").text().equals(null))) {

                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

                        String date = doc.select("dd.debut").select("div.desc").text();

                        LocalDate localDate = LocalDate.parse(date, formatter);

                        player.setDebutDate(localDate);
                    }

                    player.setDraftOrder(doc.select("dd.draft").select("div.desc").text());

                    player.setDegree(doc.select("dd.edu").select("div.desc").text());

                    player.setNation(doc.select("dd.nationality").select("div.desc").text());

                    player.setOriginName(doc.select("dd.original_name").select("div.desc").text());

                    player.setPopularity(0);

                    playerService.save(player);

                    presentresult = "\n"
                            + playernumber + ","
                            + repalcedPlayername + ","
                            + doc.select("dd.pos").select("div.desc").text() + ","
                            + doc.select("dd.b_t").select("div.desc").text() + ","
                            + Integer.valueOf(height_and_weight_divided[0]) + ","//
                            + Integer.valueOf(height_and_weight_divided[1]) + ","//
                            + doc.select("dd.born").select("div.desc").text() + ","
                            + doc.select("dd.debut").select("div.desc").text() + ","
                            + doc.select("dd.edu").select("div.desc").text() + ","
                            + doc.select("dd.nationality").select("div.desc").text() + ","
                            + doc.select("dd.original_name").select("div.desc").text() + ","
                            + doc.select("dd.draft").select("div.desc").text();
                    log.info(presentresult);
                }
            }
        } catch (Exception e) {
            log.error(Constants.BOOM, e);
        }
    }
}