package baseballrobot.service;

import baseballrobot.entity.Player;
import baseballrobot.entity.User;
import baseballrobot.enumerated.UserStatus;
import baseballrobot.model.LineReply;
import baseballrobot.model.LineReplyMessage;
import baseballrobot.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

//import static baseballrobot.enumerated.AskingStatus.*;

@Slf4j
@Service
public class AskingService {
    @Autowired
    private QuickReplyButtonService quickReplyButtonService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private UserService userService;
    @Autowired
    private PlayerPhotoService playerPhotoService;
    @Autowired
    private PassionAtBatService passionAtBatService;
    @Value("${cpblwebsite.testPictureUrl}")
    private String testPictureUrl;
    @Value("${linerobot.webhookPrefix}")
    private String webhookPrefix;
    @Value("${linerobot.noPlayerImageUrl}")
    private String linerobotNoPlayerImageUrl;

    LineReply isNonZeroDoubleDigitNumber(User user, LineReply replyDto, String ask) {
        int askInt = Integer.parseInt(ask);
        if ((askInt > user.getListLength()) && (askInt <= 0)) {
            return getCustomizedWrongFormatDto(Constants.INSERTED_NUMBER_BIGGER_THAN_LIST_LENGTH, user, replyDto);
        }
        if (!user.getPlayerInformation().isBlank()) {
            List<Player> playerList = new ArrayList();
            playerList.add(playerService.findAllByName(user.getPlayerName()).get(askInt - 1));
            return getInformationDto(user, playerList, replyDto, ask);
        } else {
            user.setSameNamePlayerNumber(askInt);
            userService.save(user);
            replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.WHAT_INFORMATION_DO_YOU_WANT_TO_FIND,
                    quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
            return replyDto;
        }
    }

    LineReply noCommaNeitherNameNorInformation(User user, LineReply replyDto) {
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.WRONG_FORMAT,
                quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    LineReply noCommaOnlyNameInformationEmptyNoSameName(User user, LineReply replyDto, String ask) {
        user.setPlayerName(ask);
        userService.save(user);
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.WHAT_INFORMATION_DO_YOU_WANT_TO_FIND,
                quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    LineReply noCommaOnlyNameInformationFilledExistSameName(User user, LineReply replyDto, String ask) {
        List<Player> playerList = playerService.findAllByName(ask);
        user.setListLength(playerList.size());
        user.setPlayerName(ask);
        userService.save(user);
        return getSameNamePlayerListDto(user, replyDto, ask);
    }

    LineReply noCommaOnlyNameInformationFilledNoSameName(User user, LineReply replyDto, String ask) {
        List<Player> playerList = playerService.findAllByName(ask);
        return getInformationDto(user, playerList, replyDto, ask);
    }

    LineReply noCommaOnlyNameInformationEmptyExistSameName(User user, LineReply replyDto, String name) {
        List<Player> playerList = playerService.findAllByName(name);
        user.setListLength(playerList.size());
        user.setPlayerName(name);
        userService.save(user);
        return getSameNamePlayerListDto(user, replyDto, name);
    }

    LineReply noCommaOnlyInformationNameFilledNoSameName(User user, LineReply replyDto, String ask) {
        List<Player> playerList = playerService.findAllByName(user.getPlayerName());
        return getInformationDto(user, playerList, replyDto, ask);
    }

    LineReply noCommaOnlyInformationNameFilledExistSameName(User user, LineReply replyDto, String ask) {
        if ((user.getListLength() >= 2) && (user.getSameNamePlayerNumber() != 0)) {
            List<Player> playerList = playerService.findAllByName(user.getPlayerName());
            Player player = playerList.get(user.getSameNamePlayerNumber() - 1);
            playerList.clear();
            playerList.add(player);
            return getInformationDto(user, playerList, replyDto, ask);
        }
        List<Player> playerList = playerService.findAllByName(ask);
        user.setListLength(playerList.size());
        user.setPlayerName(ask);
        userService.save(user);
        return getSameNamePlayerListDto(user, replyDto, ask);
    }

    LineReply noCommaOnlyInformationNameEmpty(User user, LineReply replyDto, String ask) {
        user.setPlayerInformation(ask);
        userService.save(user);
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.WHICH_PLAYER_DO_YOU_WANT_TO_FIND,
                quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    LineReply containsCommaBothAreRightNoSameName(User user, LineReply replyDto, String ask) {
        String[] askArray = ask.split(",");
        String playerName = askArray[0];
        String playerInformation = askArray[1];
        List<Player> playerList = playerService.findAllByName(playerName);
        return getInformationDto(user, playerList, replyDto, playerInformation);
    }

    LineReply containsCommaBothAreRightExistSameName(User user, LineReply replyDto, String ask) {
        String[] askArray = ask.split(",");
        List<Player> playerList = playerService.findAllByName(askArray[0]);
        user.setListLength(playerList.size());
        user.setPlayerInformation(askArray[1]);
        user.setPlayerName(askArray[0]);
        userService.save(user);
        return getSameNamePlayerListDto(user, replyDto, askArray[0]);
    }

    LineReply containsCommaNameIsRight(User user, LineReply replyDto, String ask) {
        String[] askArray = ask.split(",");
        user.setPlayerName(askArray[0]);
        userService.save(user);
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(
                Constants.INFORMATION_NOT_FOUND + "," + Constants.WHAT_INFORMATION_DO_YOU_WANT_TO_FIND,
                quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    LineReply containsCommaInformationIsRight(User user, LineReply replyDto, String ask) {
        String[] askArray = ask.split(",");
        user.setPlayerInformation(askArray[1]);
        userService.save(user);
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(
                Constants.PLAYER_NOT_FOUND + "," + Constants.WHICH_PLAYER_DO_YOU_WANT_TO_FIND,
                quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    LineReply getInformationDto(User user, List<Player> playerList, LineReply replyDto, String ask) {
        String answer = "";
        if (!user.getPlayerInformation().isBlank()) ask = user.getPlayerInformation();
        if (user.getSameNamePlayerNumber() != 0) {
            user.setSameNamePlayerNumber(0);
            userService.save(user);
        }
        playerList.get(0).setPopularity(playerList.get(0).getPopularity() + 1);
        switch (ask) {
            case "是否現役":
                if (playerList.get(0).getActive()) {
                    answer = Constants.YES;
                } else {
                    answer = Constants.NO;
                }
                break;
            case "守備位置":
                answer = playerList.get(0).getPosition();
                break;
            case "投打習慣":
                answer = playerList.get(0).getThrowandbat();
                break;
            case "身高":
                answer = String.valueOf(playerList.get(0).getHeight());
                break;
            case "體重":
                answer = String.valueOf(playerList.get(0).getWeight());
                break;
            case "生日":
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
                answer = playerList.get(0).getBirthDate().format(formatter);
                break;
            case "初出場":
                DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd LLLL yyyy");
                answer = playerList.get(0).getBirthDate().format(formatter2);
                break;
            case "學歷":
                answer = playerList.get(0).getDegree();
                break;
            case "國籍":
                answer = playerList.get(0).getNation();
                break;
            case "出生地":
                answer = playerList.get(0).getNation();
                break;
            case "原名":
                answer = playerList.get(0).getOriginName();
                break;
            case "選秀順位":
                answer = playerList.get(0).getDraftOrder();
                break;
            case "圖片":
                Player player = playerList.get(0);
                if (!isPlayerImageExist(player)) {
                    replyDto.getMessages().add(LineReplyMessage.createTextInstance(Constants.NO_PICTURE));
                    return replyDto;
                }
                if (player.getPhoto() == null) {
                    playerPhotoService.insertPhotoByNumber(player.getNumber());
                }
                String testImageUrl1 = playerPhotoService.searchImageUrlByPlayerNumber(player.getNumber());
                replyDto.getMessages().add(LineReplyMessage.createImageWithQuickReplyInstance(testImageUrl1, testImageUrl1
                        , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
                userService.userStatusSetClear(user);
                return replyDto;
        }
        userService.userStatusSetClear(user);
        if (StringUtils.isNotEmpty(answer)) {
            replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance((answer)
                    , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
            return replyDto;
        } else {
            replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.CPBL_OFFICIAL_WEBSITE_DONT_HAVE
                    , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
            return replyDto;
        }
    }

    private String generatePlayerImageUrl(Player player) {
        return webhookPrefix + "image/" + player.getNumber();
    }

    LineReply getSameNamePlayerListDto(User user, LineReply replyDto, String playerName) {
        List<Player> playerList = playerService.findAllByName(playerName);
        String answer = Constants.MANY_PLAYERS_HAVE_THIS_NAME_WHICH_ONE + "\n" + Constants.INSERT_A_NUMBER + "\n";
        int sameNamePlayerOrder = 0;
        for (Player player : playerList) {
            sameNamePlayerOrder++;
            answer = answer + sameNamePlayerOrder + "." + player.getName() + player.getBirthDate();
            if (sameNamePlayerOrder < playerList.size()) answer = answer + "\n";
        }
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(answer
                , quickReplyButtonService.createNumberQuickReplyButton(playerList.size())));
        return replyDto;
    }

    LineReply getWrongFormatDto(User user, LineReply replyDto) {
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.WRONG_FORMAT,
                quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    LineReply getCustomizedWrongFormatDto(String customizedWords, User user, LineReply replyDto) {
        String customizedWordsAndWrongFormatWarning = Constants.WRONG_FORMAT + ", " + customizedWords;
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(customizedWordsAndWrongFormatWarning,
                quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    LineReply getAskingBasicFunctionDto(User user, LineReply replyDto) {
        user.setStatus(UserStatus.ASKING);
        userService.save(user);
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(getAskingBasicFunctionReplyString(user)
                , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    String getAskingBasicFunctionReplyString(User user) {
        if (user.getPlayerInformation().isBlank()) {
            if (user.getPlayerName().isBlank()) return Constants.PLEASE_INSERT_PLAYERNAME_AND_INFORMATION;
            return Constants.PLEASE_INSERT_PLAYERNAME + user.getPlayerName() + "," + Constants.PLEASE_INSERT_PLAYERINFORMATION;
        }
        return Constants.YOU_ALREADY_INSERT_PLAYERINFORMATION + user.getPlayerInformation() + "," + Constants.PLEASE_INSERT_PLAYERNAME;
    }

    boolean isPlayerImageExist(Player player) {
        String playerImageUrl = playerPhotoService.searchImageUrlByPlayerNumber(player.getNumber());
        if (playerImageUrl.equals(linerobotNoPlayerImageUrl)) return false;
        return true;
    }

}