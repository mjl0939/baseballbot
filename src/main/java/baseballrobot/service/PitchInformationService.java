package baseballrobot.service;

import baseballrobot.entity.Pitch;
import baseballrobot.model.PitchScore;
import baseballrobot.model.PitchScoreResponse;
import baseballrobot.repository.PlayerRepository;
import baseballrobot.util.Constants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PitchInformationService {

    @Value("${cpblwebsite.requestVerificationToken}")
    private String requestVerificationToken;
    @Value("${cpblwebsite.getPitchScoreUrl}")
    private String getPitchScoreUrl;
    @Value("${cpblwebsite.playerEntityPrefix}")
    private String playerEntityPrefix;
    @Value("${cpblwebsite.playerEntityBottom}")
    private String playerEntityBottom;
    @Value("${playerlist.webaddress}")
    private String playerlistwebaddress;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private PitchService pitchService;

    public void insertPitchInformationToPitchTable() throws IOException {
        List<String> presentActivePlayerNumberList = new ArrayList<>();
        Document presentactivedoc = Jsoup.connect(playerlistwebaddress).timeout(10000).validateTLSCertificates(false).get();
        Elements presentactiveListElements = presentactivedoc.select("div.PlayersList");
        int i = 0;
        for (Element element : presentactiveListElements) {
            Element dlElement = element.select("dl").get(0);
            Elements ddElements = dlElement.select("dd");
            for (Element ddElement : ddElements) {
                String presentActivePlayerNumber = ddElement.select("a").get(0)
                        .attr("href").replaceAll("[^0-9]", "");

                presentActivePlayerNumberList.add(presentActivePlayerNumber);
                List<Pitch> pitchList = pitchScoreTransferToPitch(findPitchScoreList(presentActivePlayerNumber), presentActivePlayerNumber);
                for (Pitch p : pitchList) {

                    pitchService.save(p);
                }
            }
        }
    }

    public String findPitchScoreInformation(String playerNumber, String information, int year) {

        List<PitchScore> pitchScoreList = findPitchScoreList(playerNumber);

        int rookieYear = pitchScoreList.get(0).getYear();

        int listNumber = year - rookieYear;

        return findInformation(pitchScoreList.get(listNumber), information);
    }

    public List<PitchScore> findPitchScoreList(String playerNumber) {

        try {
            String url = getPitchScoreUrl;
            StringBuilder zeropart = new StringBuilder();
            HttpResponse httpResponse = null;

            HttpClient httpClient = HttpClients
                    .custom()
                    .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
                    .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .build();

            HttpPost httpPost = new HttpPost(url);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            httpPost.addHeader("Requestverificationtoken", requestVerificationToken);
            httpPost.addHeader("X-Requested-With", "XMLHttpRequest");

            String playerEntity = playerEntityPrefix + playerNumber + playerEntityBottom;
            StringEntity stringEntity = new StringEntity(playerEntity, "UTF-8");
            httpPost.setEntity(stringEntity);
            httpResponse = httpClient.execute(httpPost);

            HttpEntity httpEntity = httpResponse.getEntity();
            String transformedEntityToString = EntityUtils.toString(httpEntity, "UTF-8");

            ObjectMapper objectMapper = new ObjectMapper();
            PitchScoreResponse pitchScoreResponse = objectMapper.readValue(transformedEntityToString, PitchScoreResponse.class);

            List<PitchScore> pitchScoreList = objectMapper.readValue(pitchScoreResponse.getPitchScore(), new TypeReference<List<PitchScore>>() {
            });

            return pitchScoreList;

        } catch (Exception ex) {
            log.error(Constants.BOOM, ex);
        }

        return new ArrayList<>();
    }

    String findInformation(PitchScore pitchScore, String information) {

        switch (information) {

            case "TeamAbbrName":
                return pitchScore.getTeamAbbrName();
            case "PitchCloser":
                return String.valueOf(pitchScore.getPitchCloser());
            case "PitchStarting":
                return String.valueOf(pitchScore.getPitchStarting());
            case "Year":
                return String.valueOf(pitchScore.getYear());
            case "Loses":
                return String.valueOf(pitchScore.getLoses());
            case "CompleteGames":
                return String.valueOf(pitchScore.getCompleteGames());
            case "ReliefPointCnt":
                return String.valueOf(pitchScore.getReliefPointCnt());
            case "SaveFail":
                return String.valueOf(pitchScore.getSaveFail());
            case "SaveOK":
                return String.valueOf(pitchScore.getSaveOK());
            case "ShoutOut":
                return String.valueOf(pitchScore.getShoutout());
            case "StrikeOutCnt":
                return String.valueOf(pitchScore.getStrikeOutCnt());
            case "Wins":
                return String.valueOf(pitchScore.getWins());
        }

        return Constants.CPBL_OFFICIAL_WEBSITE_DONT_HAVE;
    }

    List<Pitch> pitchScoreTransferToPitch(List<PitchScore> pitchScoreList, String presentActivePlayerNumber) {
        List<Pitch> pitchList = new ArrayList<>();
        for (PitchScore p : pitchScoreList) {
            Pitch pitch = new Pitch();
            pitch.setNumberAndYear(presentActivePlayerNumber + p.getYear());
            pitch.setNumber(presentActivePlayerNumber);
            pitch.setPitchCloser(p.getPitchCloser());
            pitch.setPitchStarting(p.getPitchStarting());
            pitch.setName(p.getName());
            pitch.setLoses(p.getLoses());
            pitch.setWins(p.getWins());
            pitch.setReliefPointCnt(p.getReliefPointCnt());
            pitch.setCompleteGames(p.getCompleteGames());
            pitch.setSaveFail(p.getSaveFail());
            pitch.setSaveOk(p.getSaveOK());
            pitch.setShoutout(p.getShoutout());
            pitch.setTeamAbbrName(p.getTeamAbbrName());
            pitch.setStrikeoutCnt(p.getStrikeOutCnt());
            pitch.setYear(p.getYear());
            pitchList.add(pitch);
        }
        return pitchList;
    }


}
