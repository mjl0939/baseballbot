package baseballrobot.service;

import baseballrobot.entity.User;
import baseballrobot.enumerated.PassionAtBatStatus;
import baseballrobot.enumerated.UserStatus;
import baseballrobot.model.LineEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Slf4j
@Service
public class LineBotFollowHandlerService {
    @Autowired
    private LineReplyService lineReplyService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private UserService userService;
    @Value("${linerobot.rule}")
    private String linerobotRule;
    @Value("${linerobot.greeting}")
    private String linerobotGreeting;

    public void handleEvent(LineEvent event) {
        Optional<User> optuser;
        User user;
        String userId;
        userId = event.getSource().getUserId();
        optuser = userService.findAllByUserId(userId);
        user = new User();
        if (optuser.isPresent()) user = optuser.get();
        if (optuser.isEmpty()) {
            user.setUserId(userId);
            user.setStatus(UserStatus.ASKING);
            user.setPlayerName("");
            user.setPlayerInformation("");
            user.setListLength(0);
            user.setSameNamePlayerNumber(0);
            user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_HEIGHT);
            user.setHeight("");
            user.setRightyOrLefty("");
            user.setHitter("陳子豪");
            user.setPitcher("");
            userService.save(user);
        }
        String firstspeak = linerobotGreeting + linerobotRule;
        lineReplyService.reply(event.getReplyToken(), Arrays.asList(firstspeak));
    }
}