package baseballrobot.service;

import baseballrobot.entity.Player;
import baseballrobot.entity.User;
import baseballrobot.enumerated.BasicFunctionStatus;
import baseballrobot.enumerated.UserStatus;
import baseballrobot.model.LineEvent;
import baseballrobot.model.LineReply;
import baseballrobot.model.LineReplyMessage;
import baseballrobot.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.*;

@Slf4j
@Service
public class LineBotMessageHandlerService {
    @Autowired
    private CheckingService checkingService;
    @Autowired
    private QuickReplyButtonService quickReplyButtonService;
    @Autowired
    private LineReplyService lineReplyService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private UserService userService;
    @Autowired
    AskingService askingService;
    @Autowired
    PassionAtBatService passionAtBatService;
    @Value("${linerobot.rule}")
    private String linerobotRule;

    public void handleEvent(LineEvent event) {
        LineReply replyDto = getPlayerInformation(event);
        lineReplyService.reply(replyDto);
    }

    LineReply getPlayerInformation(LineEvent event) {
        LineReply replyDto = new LineReply();
        replyDto.setReplyToken(event.getReplyToken());
        User user = new User();
        Optional<User> optuser;
        String userId;
        userId = event.getSource().getUserId();
        optuser = userService.findAllByUserId(userId);
        if (optuser.isPresent()) user = optuser.get();
        try {
            String ask;
            ask = event.getMessage().getText();
            switch (whatTypeOfBasicFunction(ask)) {
                case RULE:
                    return getRuleBasicFunctionDto(user, replyDto);
                case WHO_HAS_BIRTHDAY_TODAY:
                    return getWhoHasBirthdayTodayBasicFunctionDto(user, replyDto);
                case ASKING:
                    return getAskingBasicFunctionDto(user, replyDto);
                case PASSION_AT_BAT:
                    return getPassionAtBatBasicFunctionDto(user, replyDto);
            }
            switch (user.getStatus()) {
                case ASKING:
                    return getAskingDto(user, replyDto, ask);
                case PASSION_AT_BAT:
                    return getPassionAtBatDto(ask, user, replyDto);
            }
        } catch (Exception e) {
            log.error(Constants.BOOM, e);
        }
        return askingService.getWrongFormatDto(user, replyDto);
    }

    LineReply getAskingBasicFunctionDto(User user, LineReply replyDto) {
        return askingService.getAskingBasicFunctionDto(user, replyDto);
    }

    LineReply getAskingDto(User user, LineReply replyDto, String ask) {
        return checkingService.getAskingDto(user, replyDto, ask);
    }

    LineReply getWhoHasBirthdayTodayBasicFunctionDto(User user, LineReply replyDto) {
        String birthday = "";
        String answer = "";
        if (LocalDate.now().getMonthValue() <= 9) birthday = birthday + "0";
        birthday = birthday + LocalDate.now().getMonthValue();
        if (LocalDate.now().getDayOfMonth() <= 9) birthday = birthday + "0";
        birthday = birthday + LocalDate.now().getDayOfMonth();
        List<Player> birthdayPlayerList = playerService.findByBirthday(birthday);
        if (!birthdayPlayerList.isEmpty()) {
            if (birthdayPlayerList.size() > 1) {
                int birthdayPlayerOrder = 0;
                for (Player player : birthdayPlayerList) {
                    birthdayPlayerOrder++;
                    answer = answer + birthdayPlayerOrder + "." + player.getName() + player.getBirthDate();
                    if (birthdayPlayerOrder < birthdayPlayerList.size()) answer = answer + "\n";
                }
                replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(answer
                        , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
                return replyDto;
            }
            Player player = birthdayPlayerList.get(0);
            answer = player.getName() + player.getBirthDate();
            replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(answer
                    , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
            return replyDto;
        } else {
            answer = Constants.NO_ONE_HAS_BIRTHDAY_TODAY;
            replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(answer
                    , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
            return replyDto;
        }
    }

    LineReply getRuleBasicFunctionDto(User user, LineReply replyDto) {
        String rule = linerobotRule;
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(rule
                , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }

    LineReply getPassionAtBatBasicFunctionDto(User user, LineReply replyDto) throws NoSuchAlgorithmException, KeyStoreException, JsonProcessingException {
        if( (user.getSameNamePlayerNumber()==0) && (user.getListLength()==0) ) {
        return passionAtBatService.getPassionAtBatBasicFunctionDto(user, replyDto);
        }
        return askingService.getCustomizedWrongFormatDto(Constants.PLEASE_KEEP_CONFIRMING_SAME_NAME_PLAYER, user, replyDto);
    }

    BasicFunctionStatus whatTypeOfBasicFunction(String ask) {
        return checkingService.whatTypeOfBasicFunction(ask);
    }

    private LineReply getPassionAtBatDto(String ask, User user, LineReply replyDto) throws NoSuchAlgorithmException, KeyStoreException, JsonProcessingException {
        if( (user.getSameNamePlayerNumber()==0) && (user.getListLength()==0) ) {
            user.setStatus(UserStatus.PASSION_AT_BAT);
            userService.save(user);
            return checkingService.getPassionAtBatDto(ask, user, replyDto);
        }
        return askingService.getCustomizedWrongFormatDto(Constants.PLEASE_KEEP_CONFIRMING_SAME_NAME_PLAYER, user, replyDto);
    }
}