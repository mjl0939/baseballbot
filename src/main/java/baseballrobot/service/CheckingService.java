package baseballrobot.service;

import baseballrobot.entity.Player;
import baseballrobot.entity.User;
import baseballrobot.enumerated.AskingStatus;
import baseballrobot.enumerated.BasicFunctionStatus;
import baseballrobot.enumerated.PassionAtBatStatus;
import baseballrobot.model.LineReply;
import baseballrobot.model.LineReplyMessage;
import baseballrobot.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static baseballrobot.enumerated.AskingStatus.*;


@Slf4j
@Service
public class CheckingService {
    @Autowired
    PlayerPhotoService playerPhotoService;
    @Autowired
    PlayerService playerService;
    @Autowired
    UserService userService;
    @Autowired
    QuickReplyButtonService quickReplyButtonService;
    @Autowired
    PassionAtBatService passionAtBatService;
    @Autowired
    AskingService askingService;
    @Value("${linerobot.noPlayerImageUrl}")
    private String linerobotNoPlayerImageUrl;

    boolean isHeightAnswerWithRightFormat(String ask) {
        switch (ask) {
            case "< 175":
                return true;
            case "175~180":
                return true;
            case "180~185":
                return true;
            case "照上次資料":
                return true;
            default:
                return false;
        }
    }

    boolean isRightyLeftyAnswerWithRightFormat(String ask) {
        if (ask.equals(Constants.RIGHTY) || ask.equals(Constants.LEFTY)) return true;
        return false;
    }

    boolean isPitcherAnswerWithRightFormat(String ask) {
        List<Player> playerList = new ArrayList<>();
        playerList.addAll(playerService.findStar());
        for (Player p : playerList) {
            if (ask.equals(p.getName())) return true;
        }
        return false;
    }

    boolean isPlayerImageExist(Player player) {

        String playerImageUrl = playerPhotoService.searchImageUrlByPlayerNumber(player.getNumber());

        if (playerImageUrl.equals(linerobotNoPlayerImageUrl)) return false;

        return true;
    }

    public boolean isInformation(String information) {

        return getInformationList().contains(information);
    }

    public boolean isPlayerName(String playerName) {
        if ((playerService.findAllByName(playerName) == null) || (playerService.findAllByName(playerName).isEmpty()))
            return false;
        return true;
    }

    public List<String> getInformationList() {
        List<String> informationList = new ArrayList<>();
        informationList.add(Constants.ACTIVE);
        informationList.add(Constants.POSITION);
        informationList.add(Constants.THORW_AND_BAT);
        informationList.add(Constants.HEIGHT);
        informationList.add(Constants.WEIGHT);
        informationList.add(Constants.BIRTH_DATE);
        informationList.add(Constants.DEBUT_DATE);
        informationList.add(Constants.DEGREE);
        informationList.add(Constants.NATION);
        informationList.add(Constants.BIRTH_PLACE);
        informationList.add(Constants.ORIGINAL_NAME);
        informationList.add(Constants.DRAFT);
        informationList.add(Constants.PHOTO);
        informationList.add(Constants.HOW_MANY_PITCH_GAME_IN_ROOKIE_YEAR);
        return informationList;
    }

    public List<String> getTestList() {
        List<String> testList = new ArrayList<>();
        testList.add(Constants.MAYAW_PAONG_BIRTHDAY);
        testList.add(Constants.TWO);
        testList.add(Constants.MAYAW_PAONG);
        testList.add(Constants.LIN_EN_YU_HEIGHT);
        testList.add(Constants.RULE);
        testList.add(Constants.WHO_HAS_BIRTHDAY_TODAY);
        return testList;
    }

    LineReply getPassionAtBatDto(String ask, User user, LineReply replyDto) throws NoSuchAlgorithmException, KeyStoreException
            , JsonProcessingException {
        switch (user.getPassionAtBatStatus()) {
            case CHOOSE_HEIGHT:
                if (isHeightAnswerWithRightFormat(ask)) {
                    if (ask.equals(Constants.FOLLOW_LAST_INSERT)) {
                        user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_PITCHER);
                        userService.save(user);
                        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.PLEASE_CHOOSE_PITCHER
                                , quickReplyButtonService.createChoosePitcherQuickReplyButton()));
                        return replyDto;
                    }
                    user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_RIGHTY_OR_LEFTY);
                    user.setHeight(ask);
                    userService.save(user);
                    replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(
                            Constants.PLEASE_CHOOSE_WHETHER_LEFTY_OR_RIGHTY_YOU_ARE
                            , quickReplyButtonService.createChooseRightyLeftyQuickReplyButton()));
                    return replyDto;
                } else {
                    replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.PLEASE_PRESS_BUTTON_TO_CHOOSE_THANKS
                            , quickReplyButtonService.createChooseHeightQuickReplyButton(user)));
                    return replyDto;
                }
            case CHOOSE_RIGHTY_OR_LEFTY:
                if (isRightyLeftyAnswerWithRightFormat(ask)) {
                    user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_PITCHER);
                    user.setRightyOrLefty(ask);
                    userService.save(user);
                    replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.PLEASE_CHOOSE_PITCHER
                            , quickReplyButtonService.createChoosePitcherQuickReplyButton()));
                    return replyDto;
                } else {
                    replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.PLEASE_PRESS_BUTTON_TO_CHOOSE_THANKS
                            , quickReplyButtonService.createChooseRightyLeftyQuickReplyButton()));
                    return replyDto;
                }
            case CHOOSE_PITCHER:
                if (isPitcherAnswerWithRightFormat(ask)) {
                    user.setPassionAtBatStatus(PassionAtBatStatus.CHOOSE_HEIGHT);
                    user.setPitcher(ask);
                    userService.save(user);
                    String pitcherNumber = "";
                    String hitterNumber = "";
                    for (Player player : playerService.findStar()) {
                        if (player.getName().equals(user.getPitcher())) pitcherNumber = player.getNumber();
                        if (player.getName().equals(user.getHitter())) hitterNumber = player.getNumber();
                    }
                    String result = Constants.RESULT_OF_AT_BAT + "\n"
                            + passionAtBatService.simulateAtBatResult(hitterNumber, pitcherNumber);
                    userService.userStatusSetClear(user);
                    replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(result
                            , quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
                    return replyDto;
                } else {
                    replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.PLEASE_PRESS_BUTTON_TO_CHOOSE_THANKS
                            , quickReplyButtonService.createChoosePitcherQuickReplyButton()));
                    return replyDto;
                }
        }
        return replyDto;
    }

    BasicFunctionStatus whatTypeOfBasicFunction(String ask) {
        if (ask.equals(Constants.RULE))
            return BasicFunctionStatus.RULE;
        if (ask.equals(Constants.WHO_HAS_BIRTHDAY_TODAY))
            return BasicFunctionStatus.WHO_HAS_BIRTHDAY_TODAY;
        if (ask.equals(Constants.ASKING))
            return BasicFunctionStatus.ASKING;
        if (ask.equals(Constants.PASSION_AT_BAT))
            return BasicFunctionStatus.PASSION_AT_BAT;
        return BasicFunctionStatus.NOTHING;
    }

    LineReply getAskingDto(User user, LineReply replyDto, String ask) {
        if (whatTypeOfNumber(ask) == NON_ZERO_DOUBLE_DIGIT_NUMBER)
            return isNonZeroDoubleDigitNumber(user, replyDto, ask);
        switch (whatTypeOfContainsComma(ask)) {
            case CONTAINS_COMMA:
                return containsComma(user, replyDto, ask);
            case NO_COMMA:
                return noComma(user, replyDto, ask);
        }
        return askingService.getWrongFormatDto(user, replyDto);
    }

    LineReply containsComma(User user, LineReply replyDto, String ask) {
        switch (whatTypeOfNameAndInformationWhenContainsComma(ask, user)) {
            case NEITHER_NAME_NOR_INFORMATION:
                return containsCommaNeitherNameNorInformation(user, replyDto);
            case BOTH_ARE_RIGHT:
                return containsCommaAndBothAreRight(user, replyDto, ask);
            case NAME_IS_RIGHT:
                return containsCommaNameIsRight(user, replyDto, ask);
            case INFORMATION_IS_RIGHT:
                return containsCommaInformationIsRight(user, replyDto, ask);
        }
        return null;
    }

    LineReply containsCommaAndBothAreRight(User user, LineReply replyDto, String ask) {
        switch (whatTypeOfSameNameWhenContainsCommaAndBothAreRight(ask, user)) {
            case NO_SAME_NAME:
                return containsCommaBothAreRightNoSameName(user, replyDto, ask);
            case SAME_NAME_EXIST:
                return containsCommaBothAreRightExistSameName(user, replyDto, ask);
        }
        return null;
    }

    LineReply noComma(User user, LineReply replyDto, String ask) {
        switch (whatTypeOfNameAndInformationWhenNoComma(ask)) {
            case NEITHER_NAME_NOR_INFORMATION:
                return noCommaNeitherNameNorInformation(user, replyDto);
            case ONLY_NAME:
                return noCommaAndOnlyName(user, replyDto, ask);
            case ONLY_INFORMATION:
                return noCommaAndOnlyInformation(user, replyDto, ask);
        }
        return null;
    }

    LineReply noCommaAndOnlyName(User user, LineReply replyDto, String ask) {
        switch (whatTypeOfInformationWhenNoCommaAndOnlyName(user)) {
            case INFORMATION_FILLED:
                return doWhenNoCommaAndOnlyNameWithInformationFilled(user, replyDto, ask);
            case INFORMATION_EMPTY:
                return doWhenNoCommaAndOnlyNameWithInformationEmpty(user, replyDto, ask);
        }
        return null;
    }

    LineReply doWhenNoCommaAndOnlyNameWithInformationFilled(User user, LineReply replyDto, String ask) {
        switch (whatTypeOfSameNameWhenNoCommaAndOnlyNameWithInformationFilled(ask)) {
            case NO_SAME_NAME:
                return noCommaOnlyNameInformationFilledNoSameName(user, replyDto, ask);
            case SAME_NAME_EXIST:
                return noCommaOnlyNameInformationFilledExistSameName(user, replyDto, ask);
        }
        return null;
    }

    LineReply doWhenNoCommaAndOnlyNameWithInformationEmpty(User user, LineReply replyDto, String ask) {
        switch (whatTypeOfSameNameWhenNoCommaAndOnlyNameWithInformationEmpty(ask)) {
            case NO_SAME_NAME:
                return noCommaOnlyNameInformationEmptyNoSameName(user, replyDto, ask);
            case SAME_NAME_EXIST:
                return noCommaOnlyNameInformationEmptyExistSameName(user, replyDto, ask);
        }
        return null;
    }

    LineReply noCommaAndOnlyInformation(User user, LineReply replyDto, String ask) {
        switch (whatTypeOfNameWhenNoCommaAndOnlyInformation(user)) {
            case NAME_FILLED:
                doWhenNoCommaAndOnlyInformationWithNameFilled(user, replyDto, ask);
            case NAME_EMPTY:
                noCommaOnlyInformationNameEmpty(user, replyDto, ask);
        }
        return null;
    }

    void doWhenNoCommaAndOnlyInformationWithNameFilled(User user, LineReply replyDto, String ask) {
        switch (whatTypeOfSameNameWhenNoCommaAndOnlyNameWithInformationFilled(ask)) {
            case NO_SAME_NAME:
                noCommaOnlyInformationNameFilledNoSameName(user, replyDto, ask);
                return;
            case SAME_NAME_EXIST:
                noCommaOnlyInformationNameFilledExistSameName(user, replyDto, ask);
        }
    }

    AskingStatus whatTypeOfTestListContains(String ask) {
        if (doesTestListContains(ask)) return TESTLIST_CONTAINS;
        return AskingStatus.NOTHING;
    }

    boolean doesTestListContains(String ask) {
        List<String> testList = getTestList();
        return testList.contains(ask);
    }

    AskingStatus whatTypeOfNumber(String ask) {
        if ((ask.length() == 1) && (ask.matches("^[1-9]*$"))) return AskingStatus.NON_ZERO_DOUBLE_DIGIT_NUMBER;
        return AskingStatus.NOTHING;
    }

    LineReply isNonZeroDoubleDigitNumber(User user, LineReply replyDto, String ask) {
        return askingService.isNonZeroDoubleDigitNumber(user, replyDto, ask);
    }

    AskingStatus whatTypeOfContainsComma(String ask) {
        if (ask.contains(",")) return AskingStatus.CONTAINS_COMMA;
        return AskingStatus.NO_COMMA;
    }

    AskingStatus whatTypeOfNameAndInformationWhenContainsComma(String ask, User user) {
        String[] question = ask.split(",");
        String playerName = question[0];
        String playerInformation = question[1];
        boolean doPlayerNameExist = isPlayerName(playerName);
        boolean doInformationExist = isInformation(playerInformation);
        if (doPlayerNameExist) {
            if (doInformationExist) return AskingStatus.BOTH_ARE_RIGHT;
            return AskingStatus.NAME_IS_RIGHT;
        } else {
            if (doInformationExist) return AskingStatus.INFORMATION_IS_RIGHT;
            return AskingStatus.NEITHER_NAME_NOR_INFORMATION;
        }
    }

    LineReply containsCommaNeitherNameNorInformation(User user, LineReply replyDto) {
        return getWrongFormatDto(user, replyDto);
    }

    AskingStatus whatTypeOfSameNameWhenContainsCommaAndBothAreRight(String ask, User user) {
        String[] question = ask.split(",");
        String playerName = question[0];
        List<Player> playerList = playerService.findAllByName(playerName);
        if (playerList.size() == 1) return AskingStatus.NO_SAME_NAME;
        return AskingStatus.SAME_NAME_EXIST;
    }

    LineReply containsCommaBothAreRightNoSameName(User user, LineReply replyDto, String ask) {
        return askingService.containsCommaBothAreRightNoSameName(user, replyDto, ask);
    }

    LineReply containsCommaBothAreRightExistSameName(User user, LineReply replyDto, String ask) {
        return askingService.containsCommaBothAreRightExistSameName(user, replyDto, ask);
    }

    LineReply containsCommaNameIsRight(User user, LineReply replyDto, String ask) {
        return askingService.containsCommaNameIsRight(user, replyDto, ask);
    }

    LineReply noCommaNeitherNameNorInformation(User user, LineReply replyDto) {
        return askingService.noCommaNeitherNameNorInformation(user, replyDto);
    }

    LineReply noCommaOnlyNameInformationEmptyNoSameName(User user, LineReply replyDto, String ask) {
        return askingService.noCommaOnlyNameInformationEmptyNoSameName(user, replyDto, ask);
    }

    LineReply noCommaOnlyNameInformationFilledExistSameName(User user, LineReply replyDto, String ask) {
        return askingService.noCommaOnlyNameInformationFilledExistSameName(user, replyDto, ask);
    }

    LineReply noCommaOnlyNameInformationFilledNoSameName(User user, LineReply replyDto, String ask) {
        return askingService.noCommaOnlyNameInformationFilledNoSameName(user, replyDto, ask);
    }

    LineReply noCommaOnlyNameInformationEmptyExistSameName(User user, LineReply replyDto, String name) {
        return askingService.noCommaOnlyNameInformationEmptyExistSameName(user, replyDto, name);
    }

    LineReply noCommaOnlyInformationNameFilledNoSameName(User user, LineReply replyDto, String ask) {
        return askingService.noCommaOnlyInformationNameFilledNoSameName(user, replyDto, ask);
    }

    LineReply noCommaOnlyInformationNameFilledExistSameName(User user, LineReply replyDto, String ask) {
        return askingService.noCommaOnlyInformationNameFilledExistSameName(user, replyDto, ask);
    }

    LineReply noCommaOnlyInformationNameEmpty(User user, LineReply replyDto, String ask) {
        return askingService.noCommaOnlyInformationNameEmpty(user, replyDto, ask);
    }

    LineReply containsCommaInformationIsRight(User user, LineReply replyDto, String ask) {
        return askingService.containsCommaInformationIsRight(user, replyDto, ask);
    }

    AskingStatus whatTypeOfNameAndInformationWhenNoComma(String ask) {
        boolean isPlayerName = isPlayerName(ask);
        boolean isInformation = isInformation(ask);
        if (isPlayerName) return AskingStatus.ONLY_NAME;
        if (isInformation) return AskingStatus.ONLY_INFORMATION;
        return AskingStatus.NEITHER_NAME_NOR_INFORMATION;
    }

    AskingStatus whatTypeOfInformationWhenNoCommaAndOnlyName(User user) {
        if (user.getPlayerInformation().isBlank()) return AskingStatus.INFORMATION_EMPTY;
        return AskingStatus.INFORMATION_FILLED;
    }

    AskingStatus whatTypeOfSameNameWhenNoCommaAndOnlyNameWithInformationEmpty(String ask) {
        List<Player> playerList = playerService.findAllByName(ask);
        if (playerList.size() == 1) return AskingStatus.NO_SAME_NAME;
        return AskingStatus.SAME_NAME_EXIST;
    }

    AskingStatus whatTypeOfNameWhenNoCommaAndOnlyInformation(User user) {
        if ((user.getPlayerName() == null) || (user.getPlayerName().isEmpty())) return AskingStatus.NAME_EMPTY;
        return AskingStatus.NAME_FILLED;
    }

    AskingStatus whatTypeOfSameNameWhenNoCommaAndOnlyNameWithInformationFilled(String ask) {
        List<Player> playerList = playerService.findAllByName(ask);
        if (playerList.size() == 1) return AskingStatus.NO_SAME_NAME;
        return AskingStatus.SAME_NAME_EXIST;
    }

    LineReply getWrongFormatDto(User user, LineReply replyDto) {
        replyDto.getMessages().add(LineReplyMessage.createQuickReplyInstance(Constants.WRONG_FORMAT,
                quickReplyButtonService.createAllFunctionQuickReplyButton(user)));
        return replyDto;
    }
}
