package baseballrobot.service;

import baseballrobot.entity.Player;
import baseballrobot.entity.User;
import baseballrobot.model.LineEventMessage;
import baseballrobot.model.LineEventSource;
import baseballrobot.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class QuickReplyButtonService {
    @Autowired
    PlayerService playerService;
    @Autowired
    UserService userService;

    LineEventSource.QuickReplyItemsDto createChooseHeightQuickReplyButton(User user) {
        LineEventSource.QuickReplyItemsDto quickReplyItemsDto = new LineEventSource.QuickReplyItemsDto();
        List<LineEventMessage.QuickReplyButtonDto> quickReplyButtonDtoList = new ArrayList<>();

        LineEventMessage.QuickReplyActionDto followBelowOneSeventeenFiveQuickReplyActionDto = new LineEventMessage.QuickReplyActionDto();
        LineEventMessage.QuickReplyButtonDto followBelowOneSeventeenFiveQuickReplyButtonDto = new LineEventMessage.QuickReplyButtonDto();
        followBelowOneSeventeenFiveQuickReplyActionDto = followBelowOneSeventeenFiveQuickReplyActionDto.createInstance(
                Constants.BELLOW_ONE_SEVENTEEN_FIVE, Constants.BELLOW_ONE_SEVENTEEN_FIVE);
        followBelowOneSeventeenFiveQuickReplyButtonDto = followBelowOneSeventeenFiveQuickReplyButtonDto.createInstance(
                followBelowOneSeventeenFiveQuickReplyActionDto);
        quickReplyButtonDtoList.add(followBelowOneSeventeenFiveQuickReplyButtonDto);
        for (int i = 175; i < 185; i = i + 5) {
            LineEventMessage.QuickReplyActionDto quickReplyActionDto = new LineEventMessage.QuickReplyActionDto();
            LineEventMessage.QuickReplyButtonDto quickReplyButtonDto = new LineEventMessage.QuickReplyButtonDto();
            quickReplyActionDto = quickReplyActionDto.createInstance(i + "~" + (i + 5), i + "~" + (i + 5));
            quickReplyButtonDto = quickReplyButtonDto.createInstance(quickReplyActionDto);
            quickReplyButtonDtoList.add(quickReplyButtonDto);
        }
        if (!user.getPitcher().isEmpty() && !user.getHitter().isEmpty()) {
            LineEventMessage.QuickReplyActionDto followLastInsertQuickReplyActionDto = new LineEventMessage.QuickReplyActionDto();
            LineEventMessage.QuickReplyButtonDto followLastInsertQuickReplyButtonDto = new LineEventMessage.QuickReplyButtonDto();
            followLastInsertQuickReplyActionDto = followLastInsertQuickReplyActionDto.createInstance(Constants.FOLLOW_LAST_INSERT
                    , Constants.FOLLOW_LAST_INSERT);
            followLastInsertQuickReplyButtonDto = followLastInsertQuickReplyButtonDto.createInstance(followLastInsertQuickReplyActionDto);
            quickReplyButtonDtoList.add(followLastInsertQuickReplyButtonDto);
        }

        quickReplyItemsDto = quickReplyItemsDto.createInstance(quickReplyButtonDtoList);
        return quickReplyItemsDto;
    }

    LineEventSource.QuickReplyItemsDto createChooseRightyLeftyQuickReplyButton() {
        LineEventSource.QuickReplyItemsDto quickReplyItemsDto = new LineEventSource.QuickReplyItemsDto();
        List<LineEventMessage.QuickReplyButtonDto> quickReplyButtonDtoList = new ArrayList<>();

        LineEventMessage.QuickReplyActionDto quickReplyActionDtoRighty = new LineEventMessage.QuickReplyActionDto();
        LineEventMessage.QuickReplyButtonDto quickReplyButtonDtoRighty = new LineEventMessage.QuickReplyButtonDto();
        quickReplyActionDtoRighty = quickReplyActionDtoRighty.createInstance(Constants.LEFTY, Constants.LEFTY);
        quickReplyButtonDtoRighty = quickReplyButtonDtoRighty.createInstance(quickReplyActionDtoRighty);
        quickReplyButtonDtoList.add(quickReplyButtonDtoRighty);

        LineEventMessage.QuickReplyActionDto quickReplyActionDtoLefty = new LineEventMessage.QuickReplyActionDto();
        LineEventMessage.QuickReplyButtonDto quickReplyButtonDtoLefty = new LineEventMessage.QuickReplyButtonDto();
        quickReplyActionDtoLefty = quickReplyActionDtoLefty.createInstance(Constants.RIGHTY, Constants.RIGHTY);
        quickReplyButtonDtoLefty = quickReplyButtonDtoLefty.createInstance(quickReplyActionDtoLefty);
        quickReplyButtonDtoList.add(quickReplyButtonDtoLefty);

        quickReplyItemsDto = quickReplyItemsDto.createInstance(quickReplyButtonDtoList);
        return quickReplyItemsDto;
    }

    LineEventSource.QuickReplyItemsDto createChoosePitcherQuickReplyButton() {
        LineEventSource.QuickReplyItemsDto quickReplyItemsDto = new LineEventSource.QuickReplyItemsDto();
        List<LineEventMessage.QuickReplyButtonDto> quickReplyButtonDtoList = new ArrayList<>();
        List<Player> playerList = new ArrayList<>();
        playerList.addAll(playerService.findStar());
        for (Player p : playerList) {
            if (p.getPosition().equals(Constants.PITCHER)) {
                LineEventMessage.QuickReplyActionDto quickReplyActionDto = new LineEventMessage.QuickReplyActionDto();
                LineEventMessage.QuickReplyButtonDto quickReplyButtonDto = new LineEventMessage.QuickReplyButtonDto();
                quickReplyActionDto = quickReplyActionDto.createInstance(
                        p.getName() + "(" + p.getThrowandbat().substring(0, 2) + ")"
                        , p.getName());
                quickReplyButtonDto = quickReplyButtonDto.createInstance(quickReplyActionDto);
                quickReplyButtonDtoList.add(quickReplyButtonDto);
            }
        }
        quickReplyItemsDto = quickReplyItemsDto.createInstance(quickReplyButtonDtoList);
        return quickReplyItemsDto;
    }

    LineEventSource.QuickReplyItemsDto createAllFunctionQuickReplyButton(User user) {

        LineEventSource.QuickReplyItemsDto quickReplyItemsDto = new LineEventSource.QuickReplyItemsDto();

        List<LineEventMessage.QuickReplyButtonDto> quickReplyButtonDtoList = new ArrayList<>();

        quickReplyButtonDtoList.add(createQuickReplyButtonDto(Constants.RULE, Constants.RULE));
        quickReplyButtonDtoList.add(createQuickReplyButtonDto(Constants.WHO_HAS_BIRTHDAY_TODAY, Constants.WHO_HAS_BIRTHDAY_TODAY));
        quickReplyButtonDtoList.add(createQuickReplyButtonDto(Constants.ASKING, Constants.ASKING));
        quickReplyButtonDtoList.add(createQuickReplyButtonDto(Constants.PASSION_AT_BAT, Constants.PASSION_AT_BAT));

        quickReplyButtonDtoList.add(createTestQuickReplyButtonDto(user));

        quickReplyItemsDto = quickReplyItemsDto.createInstance(quickReplyButtonDtoList);

        return quickReplyItemsDto;
    }

    private LineEventMessage.QuickReplyButtonDto createQuickReplyButtonDto(String label, String text) {
        LineEventMessage.QuickReplyActionDto quickReplyActionDto = new LineEventMessage.QuickReplyActionDto();
        LineEventMessage.QuickReplyButtonDto quickReplyButtonDto = new LineEventMessage.QuickReplyButtonDto();
        quickReplyActionDto = quickReplyActionDto.createInstance(label, text);
        quickReplyButtonDto = quickReplyButtonDto.createInstance(quickReplyActionDto);
        return quickReplyButtonDto;
    }

    LineEventSource.QuickReplyItemsDto createNumberQuickReplyButton(int howManyButtons) {
        LineEventSource.QuickReplyItemsDto quickReplyItemsDto = new LineEventSource.QuickReplyItemsDto();
        List<LineEventMessage.QuickReplyButtonDto> quickReplyButtonDtoList = new ArrayList<>();
        for (int i = 1; i <= howManyButtons; i++) {
            quickReplyButtonDtoList.add(createQuickReplyButtonDto(String.valueOf(i), String.valueOf(i)));
        }
        quickReplyButtonDtoList.add(createQuickReplyButtonDto(Constants.TEST, Constants.TWO));
        quickReplyItemsDto = quickReplyItemsDto.createInstance(quickReplyButtonDtoList);
        return quickReplyItemsDto;
    }

    LineEventMessage.QuickReplyButtonDto createTestQuickReplyButtonDto(User user) {
        if (user.getTest() == null) {
            user.setTest(1);
            userService.save(user);
        }
        switch (user.getTest()) {
            case 1:
                return createQuickReplyButtonDto(Constants.TEST, Constants.MAYAW_PAONG_BIRTHDAY);
            case 2:
                return createQuickReplyButtonDto(Constants.TEST, Constants.TWO);
            case 3:
                return createQuickReplyButtonDto(Constants.TEST, Constants.MAYAW_PAONG);
            case 4:
                return createQuickReplyButtonDto(Constants.TEST, Constants.TWO);
            case 5:
                return createQuickReplyButtonDto(Constants.TEST, Constants.LIN_EN_YU_HEIGHT);
            case 6:
                return createQuickReplyButtonDto(Constants.TEST, Constants.RULE);
            case 7:
                return createQuickReplyButtonDto(Constants.TEST, Constants.WHO_HAS_BIRTHDAY_TODAY);
            case 8:
                return createQuickReplyButtonDto(Constants.TEST, Constants.PASSION_AT_BAT);
        }

        user.setTest(1);
        userService.save(user);

        return createQuickReplyButtonDto(Constants.TEST, Constants.PASSION_AT_BAT);
    }
}