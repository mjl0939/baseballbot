package baseballrobot.service;

import baseballrobot.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class LineReplyService {

    @Value("${line.channel.accessToken}")
    private String lineChannelAccessToken;
    @Value("${linerobot.urlPrefix}")
    private String urlPrefix;

    private RestTemplate restTemplate;

    @PostConstruct
    public void postConstruct() {
        restTemplate = new RestTemplateBuilder().build();
    }

    public void reply(String replyToken, List<String> messages) {
        LineReply replyDto = LineReply.createInstance(replyToken);
        List<LineReplyMessage> replyMessages = new ArrayList<>();
        messages.forEach(m -> replyMessages.add(LineReplyMessage.createInstance("text", m)));
        replyDto.setMessages(replyMessages);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(lineChannelAccessToken);
        HttpEntity<LineReply> request = new HttpEntity<>(replyDto, headers);

        ResponseEntity<String> res = restTemplate.postForEntity(urlPrefix, request, String.class);
    }

    public void reply(LineReply replyDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(lineChannelAccessToken);
        //  headers.set("Authorization", "Bearer "+lineChannelSecret);
        HttpEntity<LineReply> request = new HttpEntity<>(replyDto, headers);

        ResponseEntity<String> res = restTemplate.postForEntity(urlPrefix, request, String.class);
    }
}
