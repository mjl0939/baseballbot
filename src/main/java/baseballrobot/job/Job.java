package baseballrobot.job;

import baseballrobot.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
public class Job {
    @Autowired
    PlayerInformationService playerInformationService;
    @Autowired
    PitchInformationService pitchInformationService;

    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.DAYS)
    public void insertPlayerInformation() throws IOException {
//        playerInformationService.insertPlayerInformationToPlayerTable();
//        pitchInformationService.insertPitchInformationToPitchTable();
    }
}
