package baseballrobot.enumerated;

public enum PassionAtBatStatus {
    CHOOSE_HEIGHT,
    CHOOSE_RIGHTY_OR_LEFTY,
    CHOOSE_PITCHER,
    SHOW_RESULT
}