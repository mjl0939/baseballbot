package baseballrobot.enumerated;

public enum BasicFunctionStatus {
    NOTHING,
    ASKING,
    PASSION_AT_BAT,
    RULE,
    WHO_HAS_BIRTHDAY_TODAY,
}